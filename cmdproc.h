/**
 * @file cmdproc.h
 * @brief Definition for the CmdProc object
 * @author Sylvester Ink
 * @version 1.0
 * @date 2021-04-28
 */

#include <string>
#include <vector>
#include <deque>
#include <unordered_map>

#include "Callback.h"

#ifndef CMDPROC_H
#define CMDPROC_H


/**
 * @brief Command processor for parsing and executing text commands.
 *        Command strings are registered along with associated callback
 *        objects.  Commands are then added to the command buffer and
 *        processed each cycle.  Each command is parsed into the function
 *        call and its associated arguments, then executed.
 */
class CmdProc
{
    public:
        using CmdArgList = std::vector<std::string>;
        using RegCBFunc = BaseCallback<void, CmdArgList&>;

    public:
        CmdProc ();
        virtual ~CmdProc ();

        void insertCmd(std::string &TextLine);
        void appendCmd(std::string &TextLine);
        void processCycle();
        void executeCmd(std::string &TextLine);
        bool isEmpty();
        void registerCB(std::string Term, RegCBFunc *CBFunc);
        void deregisterCB(std::string Term);

    private:
        std::deque<char> _Buffer;
        int _nSuspend;
        std::unordered_map<std::string, RegCBFunc*> _RegisteredCBs;

    private:
        void extractLine(std::string &TextLine);
        void parseText(std::string &TextLine, CmdArgList &Args);
        void dispatchCmd(CmdArgList &Args);

};


#endif /* end of include guard: CMDPROC_H */
