/**
 * @file Callback.h
 * @version 1.0
 * @date 2021-04-27
 * @brief Generic functor object definitions
 */

#pragma once

/**
 * @brief Abstract base class for functors
 *        The template parameter is a variable list used to define parameters
 *        of the called function.
 */
template <class R, class... T>
class BaseCallback
{
    public:
        // two possible functions to call member function. virtual cause derived
        // classes will use a pointer to an object and a pointer to a member function
        // to make the function call
        virtual R operator()(T... args)=0;   // call using operator
        virtual R Call(T... args)=0;         // call using function
};

/** ****************************************************************************
 * @brief Callback object for handling callbacks
 *        First template parameter defines the object type associated.
 *        Second template parameter is a variable list used as callback parameters.
 ******************************************************************************/
template <class R, class U, class... T>
class Callback : public BaseCallback<R, T...>
{
    public:
        /**
         * @brief Initializes the functor, storing a pointer to the object
         *        and a pointer to its member function to be called
         * @param pObject Pointer to the instantiated object
         * @param U::*pFunction Pointer to the member function of the object
         */
        Callback(U* pObject, R(U::*pFunction)(T... args))
        { m_pObject = pObject;  m_pFunction=pFunction; };

        //! Calls the member function of the object with passed args
        virtual R operator()(T... args)
        { return (*m_pObject.*m_pFunction)(args ...);};    // execute member function

        //! Calls the member function of the object with passed args
        virtual R Call(T... args)
        { return (*m_pObject.*m_pFunction)(args ...);};    // execute member function

    private:
        R (U::*m_pFunction)(T... args);      // pointer to member function
        U* m_pObject;                           // pointer to object
};

/** ****************************************************************************
 * @brief Callback object for handling callbacks to non-object functions
 *        First template parameter defines the return type
 *        Second template parameter is a variable list used as callback parameters.
 ******************************************************************************/
template <class R, class... T>
class FuncCallback : public BaseCallback<R, T...>
{
    public:
        /**
         * @brief Initializes the functor, storing a pointer to the function to be called
         * @param pFunction Pointer to the function to be called
         */
        FuncCallback(R(*pFunction)(T... args))
        { m_pFunction=pFunction; };

        //! Calls the function with passed args
        virtual R operator()(T... args)
        { return (*m_pFunction)(args ...);};    // execute function

        //! Calls the function with passed args
        virtual R Call(T... args)
        { return (*m_pFunction)(args ...);};    // execute function

    private:
        R (*m_pFunction)(T... args);      // pointer to function
};

/**
 * @brief Abstract base class specialization for functors with no return value
 */
template <class... T>
using VBaseCallback = BaseCallback<void, T...>;

/**
 * @brief Callback class specialization with no return value
 */
template <class U, class... T>
using VCallback = Callback<void, U, T...>;


// ********* CONST VERSIONS ***********
/**
 * @brief Abstract base class for functors
 *        The template parameter is a variable list used to define parameters
 *        of the called function.
 */
template <class R, class... T>
class CBaseCallback
{
    public:
        // two possible functions to call member function. virtual cause derived
        // classes will use a pointer to an object and a pointer to a member function
        // to make the function call
        virtual R operator()(T... args) const = 0;  // call using operator
        virtual R Call(T... args) const = 0;        // call using function
};

/** ****************************************************************************
 * @brief Callback object for handling callbacks
 *        First template parameter defines the object type associated.
 *        Second template parameter is a variable list used as callback parameters.
 ******************************************************************************/
template <class R, class U, class... T>
class CCallback : public CBaseCallback<R, T...>
{
    public:
        /**
         * @brief Initializes the functor, storing a pointer to the object
         *        and a pointer to its member function to be called
         * @param pObject Pointer to the instantiated object
         * @param U::*pFunction Pointer to the member function of the object
         */
        CCallback(U* pObject, R(U::*pFunction)(T... args))
        { m_pObject = pObject;  m_pFunction=pFunction; };

        //! Calls the member function of the object with passed args
        virtual R operator()(T... args) const
        { return (*m_pObject.*m_pFunction)(args ...);};    // execute member function

        //! Calls the member function of the object with passed args
        virtual R Call(T... args) const
        { return (*m_pObject.*m_pFunction)(args ...);};    // execute member function

    private:
        R (U::*m_pFunction)(T... args);      // pointer to member function
        U* m_pObject;                           // pointer to object
};

/**
 * @brief Abstract base class specialization for functors with no return value
 */
template <class... T>
using CVBaseCallback = CBaseCallback<void, T...>;

/**
 * @brief Callback class specialization with no return value
 */
template <class U, class... T>
using CVCallback = CCallback<void, U, T...>;
