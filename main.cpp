#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include "cmdproc.h"
#include "Callback.h"

static bool IS_RUNNING = true;

class TestAcc
{
    public:
        TestAcc () {}
        virtual ~TestAcc () {}

        void addCB(CmdProc::CmdArgList& Args)
        {
            if (Args.size() != 2)
            {
                std::cout << "ERROR: Incorrect Number Of Arguments (requires 1)" << std::endl;
                return;
            }

            int value = 0;
            try {
                value = std::stoi(Args[1]);
            }catch(std::invalid_argument& e) {
                std::cout << "ERROR: Invalid Argument (must be integer)" << std::endl;
                return;
            }catch(std::out_of_range& e) {
                std::cout << "ERROR: Value out of range" << std::endl;
                return;
            }

            add(value);
        }
        void printCB(CmdProc::CmdArgList& Args)
        {
            if (Args.size() != 1)
            {
                std::cout << "ERROR: Incorrect Number Of Arguments (requires 0)" << std::endl;
                return;
            }
            print();
        }

        void add(int value) { _Data += value; }
        void print() { std::cout << _Data << std::endl; }

    private:
        int _Data {0};
};

void quit(CmdProc::CmdArgList& /*Args*/)
{
    IS_RUNNING = false;
}

void action(CmdProc::CmdArgList &Args)
{
    for (unsigned int i = 0; i < Args.size(); ++i) {
        std::cout << Args[i] << std::endl;
    }
    std::cout << std::endl;
}

int main(/*int argc, char *argv[]*/)
{
    TestAcc Accumulator1;
    TestAcc Accumulator2;

    CmdProc CommandProcessor;

    FuncCallback<void, CmdProc::CmdArgList&> actionCB(action);
    FuncCallback<void, CmdProc::CmdArgList&> quitCB(quit);

    Callback<void, TestAcc, CmdProc::CmdArgList&> Accumulator1AddCB(&Accumulator1, &TestAcc::addCB);
    Callback<void, TestAcc, CmdProc::CmdArgList&> Accumulator1PrintCB(&Accumulator1, &TestAcc::printCB);

    Callback<void, TestAcc, CmdProc::CmdArgList&> Accumulator2AddCB(&Accumulator2, &TestAcc::addCB);
    Callback<void, TestAcc, CmdProc::CmdArgList&> Accumulator2PrintCB(&Accumulator2, &TestAcc::printCB);

    CommandProcessor.registerCB("action", &actionCB);
    CommandProcessor.registerCB("add1", &Accumulator1AddCB);
    CommandProcessor.registerCB("print1", &Accumulator1PrintCB);
    CommandProcessor.registerCB("add2", &Accumulator2AddCB);
    CommandProcessor.registerCB("print2", &Accumulator2PrintCB);
    CommandProcessor.registerCB("quit", &quitCB);

    std::string line;
    while (IS_RUNNING)
    {
        std::cout << "] ";
        std::getline(std::cin, line);
        CommandProcessor.appendCmd(line);
        do {
            CommandProcessor.processCycle();
        } while (!CommandProcessor.isEmpty());
    }

    return 0;
}
