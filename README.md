# Quake-Style Command Processor
A Quake-style command processor for terminal commands.

Based on a tutorial by Icculus:

    `http://www.icculus.org/~phaethon/q3/cmdproc/qcmdproc.html`

## Requirements
* C++11

## Build Instructions
`make`
