/**
 * @file cmdproc.cpp
 * @brief Implementation of the CmdProc object
 * @author Sylvester Ink
 * @version 1.0
 * @date 2021-04-28
 */

#include <sstream>

#include "cmdproc.h"


/**
 * @brief Constructor
 */
CmdProc::CmdProc() :
    _nSuspend(0)
{
}

/**
 * @brief Destructor
 */
CmdProc::~CmdProc()
{
}

/**
 * @brief Insert commands at the front the command buffer
 * @param[in] TextLine String containing commands to be inserted
 */
void CmdProc::insertCmd(std::string &TextLine)
{
    //TODO: Check for max buffer size, error if exceeded

    // Make sure ends with a semicolon
    if (TextLine.back() != ';')
    {
        _Buffer.push_front(';');
    }

    // Iterate from end of string to ensure chars inserted in correct order
    for (std::string::reverse_iterator i=TextLine.rbegin(); i!=TextLine.rend(); ++i)
    {
        // Replace newline with semicolon for command separation
        if (*i == '\n')
            _Buffer.push_front(';');
        else
            _Buffer.push_front(*i);
    }
}

/**
 * @brief Append commands to the end of the command buffer
 * @param[in] TextLine String containing commands to be appended
 */
void CmdProc::appendCmd(std::string &TextLine)
{
    //TODO: Check for max buffer size, error if exceeded

    // Iterate through characters and add to buffer
    for (std::string::iterator i=TextLine.begin(); i!=TextLine.end(); ++i)
    {
        // Replace newline with semicolon for command separation
        if (*i == '\n')
            _Buffer.push_back(';');
        else
            _Buffer.push_back(*i);
    }
}

/**
 * @brief Process the next command in the buffer
 * @note This should be called on a cyclic basis,
 *       as it will skip processing during suspend cycles.
 */
void CmdProc::processCycle()
{
    // If suspending for a number of cycles, don't do anything
    if (_nSuspend > 0)
    {
        _nSuspend--;
        // std::cout << "Waiting " << _nSuspend << " more cycles" << std::endl;
        return;
    }

    // Extract a line from the buffer and execute it
    std::string TextLine;
    extractLine(TextLine);
    //TODO: error checking
    executeCmd(TextLine);
}

/**
 * @brief Gets a command line from the buffer
 * @param[out] TextLine The string to store the extracted line to
 */
void CmdProc::extractLine(std::string &TextLine)
{
    // Move each character from buffer to TextLine
    // Only process if buffer contains data and not at end of line
    while (_Buffer.size() > 0 &&
           _Buffer.front() != ';')
    {
        TextLine.push_back(_Buffer.front());
        _Buffer.pop_front();
    }

    // Ensure endline character removed from buffer when done
    if (_Buffer.front() == ';')
        _Buffer.pop_front();
}

/**
 * @brief Parse text and execute a single command
 * @param[in] TextLine String containing text of command to execute
 */
void CmdProc::executeCmd(std::string &TextLine)
{
    CmdArgList Args;
    parseText(TextLine, Args);
    dispatchCmd(Args);
    // TODO handle returns from callbacks?
    // return dispatchCmd(Args);
}

/**
 * @brief Parse command text and tokenize args
 *        This handles special characters as well
 * @param[in] TextLine String containing command text to parse
 * @param[out] Args List of command arg strings
 */
void CmdProc::parseText(std::string &TextLine, CmdArgList &Args)
{
    std::string Token;
    bool isQuote = false;
    for (std::string::iterator i=TextLine.begin(); i!=TextLine.end(); ++i)
    {
        // If end quote or whitespace, then end current token and add to args
        if ( (isQuote && *i == '"') ||
             (!isQuote && *i <= 32) )
        {
            if (!Token.empty())
            {
                Args.push_back(Token);
                Token.clear();
            }

            isQuote = false;
        }
        // If start quote, flag this as quote to extract as single arg
        else if (*i == '"')
        {
            isQuote = true;
        }
        // If the rest is not quoted and commented out ('//'), stop parsing
        else if ( !isQuote &&
                  (*i == '/' && *(i+1) == '/') )
            break;
        // Otherwise add character as part of current token
        else
        {
            Token.push_back(*i);
        }
    }
    // Add remaining token to args when parse loop complete
    if (!Token.empty())
    {
        Args.push_back(Token);
    }
}

/**
 * @brief Execute command registered to first argument value passed
 * @param[in] Args Command and arguments. Command must be first value
 */
void CmdProc::dispatchCmd(CmdArgList &Args)
{
    // Requires first arg for command to run
    if (Args.size() >= 1)
    {
        // Make sure command exists (avoid exception)
        if (_RegisteredCBs.contains(Args.at(0)))
            _RegisteredCBs.at(Args.at(0))->Call(Args);

        // For non-c++20
        // auto cb = _RegisteredCBs.find(Args.at(0));
        // if (cb != _RegisteredCBs.end())
        //     cb->second->Call(Args);

        //TODO Send error when no command found?  Implement as callback?
    }
    return;
}

/**
 * @brief Check whether command buffer is empty
 * @return Whether command buffer is empty
 */
bool CmdProc::isEmpty()
{
    return _Buffer.empty();
}

/**
 * @brief  Register a command term and associated callback function
 *         for execution
 * @param[in] Term String containing command value
 * @param[in] CBFunc Associated callback to execute
 */
void CmdProc::registerCB(std::string Term, RegCBFunc *CBFunc)
{
    _RegisteredCBs.insert({Term, CBFunc});
}

/**
 * @brief Remove a command term and associated callback from
 *        execution map
 * @param[out] Term String containing command value to remove
 */
void CmdProc::deregisterCB(std::string Term)
{
    _RegisteredCBs.erase(Term);
}
