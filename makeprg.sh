#!/bin/bash
if [[ $1 == "init" ]]; then
    mkdir build
    pushd build
    cmake ..
    popd
else
    pushd build
    make "$@"
    popd
fi
